[package]
name = "sequoia-openpgp"
description = "OpenPGP data types and associated machinery"
version = "0.17.0"
authors = [
    "Igor Matuszewski <igor@sequoia-pgp.org>",
    "Justus Winter <justus@sequoia-pgp.org>",
    "Kai Michaelis <kai@sequoia-pgp.org>",
    "Neal H. Walfield <neal@sequoia-pgp.org>",
    "Nora Widdecke <nora@sequoia-pgp.org>",
    "Wiktor Kwapisiewicz <wiktor@sequoia-pgp.org>",
]
build = "build.rs"
documentation = "https://docs.sequoia-pgp.org/0.17.0/sequoia_openpgp"
homepage = "https://sequoia-pgp.org/"
repository = "https://gitlab.com/sequoia-pgp/sequoia"
readme = "README.md"
keywords = ["cryptography", "openpgp", "pgp", "encryption", "signing"]
categories = ["cryptography", "authentication", "email"]
license = "GPL-2.0-or-later"
edition = "2018"

[badges]
gitlab = { repository = "sequoia-pgp/sequoia" }
maintenance = { status = "actively-developed" }

[dependencies]
anyhow = "1"
buffered-reader = { path = "../buffered-reader", version = "0.17", default-features = false }
base64 = ">= 0.11, < 0.13"
bzip2 = { version = "0.3.2", optional = true }
flate2 = { version = "1.0.1", optional = true }
idna = "0.2"
lalrpop-util = "0.17"
lazy_static = "1.3"
libc = "0.2"
memsec = "0.5.6"
nettle = { version = "7", optional = true }
quickcheck = { version = "0.9", default-features = false, optional = true }
rand = { version = "0.7", default-features = false, optional = true }
regex = "1"
thiserror = "1"

# XXX: We need to pin some dependencies to keep our MSRV at 1.34.  If
# you are packaging Sequoia, feel free to drop these pins if your
# compiler is newer.
backtrace = "= 0.3.46"
unicode-normalization = "= 0.1.9"

[build-dependencies]
lalrpop = "0.17"

[dev-dependencies]
quickcheck = { version = "0.9", default-features = false }
rand = { version = "0.7", default-features = false }

# XXX: We need to pin some dependencies to keep our MSRV at 1.34.  If
# you are packaging Sequoia, feel free to drop these pins if your
# compiler is newer.
rpassword = "=4.0.3"

[features]
default = ["compression", "crypto-nettle"]
# TODO(#333): Allow for/implement more backends
crypto-nettle = ["nettle"]

# The compression algorithms.
compression = ["compression-deflate", "compression-bzip2"]
compression-deflate = ["flate2", "buffered-reader/compression-deflate"]
compression-bzip2 = ["bzip2", "buffered-reader/compression-bzip2"]

# Vendoring.
vendored = ["vendored-nettle"]
vendored-nettle = ["nettle/vendored"]

# Testing, debugging, and fuzzing.

# XXX: This feature should just be called 'quickcheck'.  However, this
# currently collides with the implicitly created feature for the
# optional dependency 'quickcheck'.  Blocker:
# https://github.com/rust-lang/cargo/issues/5565
x-quickcheck = ["quickcheck", "rand"]

[[example]]
name = "pad"
required-features = ["compression-deflate"]
